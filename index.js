const {sequelize, User, Project, ProjectMember, Bug} = require("./models.js");
const express = require("express");
const bodyParser = require("body-parser");
const passport = require("passport");
const passportLocalSequelize = require("passport-local-sequelize");
const passportLocal = require("passport-local");
const session = require("express-session");
const cors = require("cors");
const app = express();

app.use(bodyParser.json());
app.use(cors());

//SET UP PASSPORT
app.use(session({
    secret: "Dynamic TOP",
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
passport.use(new passportLocal(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());



//MIDDLEWARE - CHECKS IF THERE IS A LOGGED USER
const loggedUser = (req,res,next)=>{
    
    // if(req.isAuthenticated()){
 
        return next();
    // }else{
    //     res.status(500).send({message:"Please don't forget to login!"});
    // }
    
};



//RESET DATABASE TO THE INITIAL STATE (NO ENTRIES AT ALL)
app.get("/reset", async (req,res)=>{
   
   try{
        await sequelize.sync({force:true});
        res.status(200).send({message:"Restored database"});
   }catch(e){
       console.log(e);
       res.status(500).send({message:e.message});
   }
    
});



//REGISTER ROUTE (CREATE USER)
app.post("/register", async (req,res)=>{

   if(req.body.username && req.body.password && req.body.username.includes("@") && req.body.username.includes(".com")){
       
        try{
             await User.register(new User({username: req.body.username}), req.body.password,(e, user)=>{
                  passport.authenticate("local")(req,res,()=>{
                      res.status(200).send(req.user);
                  });
            });
        }catch(e){
           console.log(e);
           res.status(500).send({message:e.message});
        }
       
   }
   else{
       res.status(400).send({message:"Invalid user payload"});
   }
   
});



//LOGIN ROUTE
app.post("/login",passport.authenticate("local",{
    
}) ,function(req,res){
    res.status(200).send(req.user);
});



//LOGOUT ROUTE
app.get("/logout",(req, res) => {
   
   req.logout();
   res.status(200).send({message:"User successfully logged out"});
});



//===========================================
//DISPLAY ALL USERS (FOR DEBUGGING PURPOSES)
app.get("/users", loggedUser, async (req,res)=>{
    
    try{
    
        const users = await User.findAll();
        res.status(200).send({message:users});
        
    }catch(e){
        console.log(e);
        res.status(500).send({message:e.message});
    }
   
});



//CREATE A PROJECT
app.post("/projects", loggedUser, async (req,res)=>{
   console.log(req.body);
   const project = {
       name : req.body.name,
       repo_link: req.body.repo_link,
       description: req.body.description,
       UserId: req.body.userId
   };
   
   const members = req.body.members.split(",");
   
   try{
       
       const createdProject = await Project.create(project);
       
       const createdPM = await ProjectMember.create({
          is_tester: false,
          UserId: req.body.userId,
          projectId: createdProject.id
       });
       
       
       members.forEach(async(member)=>{
          
          try{
              const foundMember = await User.findOne({where: {username: member}});
              const pm = await ProjectMember.create({
                  is_tester: false,
                  UserId: foundMember.id,
                  projectId: createdProject.id
              });
              
              res.status(200).send({message:"Project created"});
          }catch(e){

              res.status(500).send({message: e.message});
          }
       });
       
              
   }catch(e){
       console.log(e);
       req.status(500).send({message:e.message});
   }
    
});



//DISPLAY ALL PROJECTS
app.get("/projects", loggedUser, async (req,res)=>{
   
   try{
       
       const foundProjects = await Project.findAll();
       res.status(200).send(foundProjects);
       
   }catch(e){
       res.status(500).send({message:e.message});
   }
    
});



//DISPLAY ALL MEMBERS - FOR DEBUGGING PURPOSES
app.get("/members", loggedUser, async (req,res)=>{
   
   try{
       
       const foundMembers = await ProjectMember.findAll();
       res.status(200).send({message:foundMembers});
       
   }catch(e){
       res.status(500).send({message:e.message});
   }
    
});



//ADD TESTER TO THE PROJECT
app.post("/projects/:id", loggedUser, async (req,res)=>{
   
   try{
       
       const existingDEVMember = await ProjectMember.findOne({where:{UserId: req.user.id, is_tester: false}});
       const exitingTSTProject = await ProjectMember.findOne({where:{UserId: req.user.id, is_tester: true, projectId: req.params.id}});
       
       if(!existingDEVMember && !exitingTSTProject){
           const newTester = await ProjectMember.create({
              is_tester: true,
              UserId: req.user.id,
              projectId: req.params.id
           });
            res.status(200).send({message:"Tester added"});
       }
       else{
           res.status(404).send({message:"User can not be a tester"});
       }
   }catch(e){
       
       res.status(500).send({message:e.message});
       
   }
    
});



//ADD BUG
app.post("/projects/:id/bugs", loggedUser, async (req, res)=>{
   
   try{
       const tester = await ProjectMember.findOne({where:{UserId:req.user.id, projectId: req.params.id, is_tester: true}});
       
        if(tester){
            try{
                const bug = {
                    projectId: req.params.id,
                    id_tester: req.user.id,
                    severity: req.body.severity,
                    priority: req.body.priority,
                    description: req.body.description,
                    issue_link: req.body.issue_link
                };
                
                const createdBug = await Bug.create(bug);
                
                res.status(200).send({message:createdBug});
                
            }catch(e){
                res.status(500).send({message:e.message});
            }
            
        }
        else{
            res.status(404).send({message:"Tester not found. Log in as a tester"});
        }
       
   }catch(e){
       res.status(500).send({message:e.message});
   }
    
});



//DISPLAY ALL BUGS - DEBUGGING PURPOSES
app.get("/bugs", loggedUser, async (req,res)=>{
   
   try{
       
       const bugs = await Bug.findAll();
       res.status(200).send({message:bugs});
       
   }catch(e){
       res.status(500).send({message: e});
   }
    
});



//DISPLAY ALL BUGS FOR A CERTAIN DEV
app.get("/projects/bugs", loggedUser, (req,res)=>{
    let allBugs = [];
    
    ProjectMember.findAll({where:{UserId: req.user.id, is_tester:false}}).then((members)=>{
        
        members.forEach((member)=>{
           
           Bug.findAll({where:{projectId:member.projectId}}).then((bugs)=>{
               
               bugs.forEach((bug)=>{
                   allBugs.push(bug);
               });
               
               res.status(200).send({message:allBugs});
               
           }).catch(e =>{
               res.status(500).send({message:e.message});
           });
            
        });
        
    }).catch(e =>{
        res.status(500).send({message:e.message});
    });
    
    
});



//BUG ALLOCATED TO DEV
app.put("/bugs/:id", loggedUser, async(req,res)=>{
   
   try{
       
       const foundBug = await Bug.findOne({where:{id:req.params.id}});
       const foundDev = await ProjectMember.findOne({where : {UserId: req.user.id, projectId: foundBug.projectId, is_tester:false}});
       
       if(foundBug && foundDev){
           
           if(!foundBug.id_dev){
               const updatedBug = await Bug.update({id_dev:req.user.id},{where: {id:foundBug.id}});
               res.status(200).send({message:"The bug is allocated to "+req.user.username});
           }
           
       }
       else{
           res.status(400).send({message:"Please log as a DEV"});
       }
       
   }catch(e){
       res.status(500).send({message:e.message});
   }
    
});



//ADD BUG SOLUTION STATUS
app.put("/bugs/:id/solution", loggedUser, async (req,res)=>{
   
   try{
       
       const foundBug = await Bug.findOne({where:{id:req.params.id}});
       const foundDev = await ProjectMember.findOne({where : {UserId: req.user.id, projectId: foundBug.projectId, is_tester:false}});
       
       if(foundBug && foundDev){
           
           if(foundBug.id_dev === foundDev.UserId){
               const updatedBug = await Bug.update({status: req.body.status, solved_link: req.body.solved_link},{where: {id:foundBug.id}});
               res.status(200).send({message:"The bug is resolved by "+req.user.username});
           }
       }
       
   }catch(e){
       res.status(500).send({message:e.message});
   }
    
});



//DELETE ONE USER
app.delete("/users", loggedUser, async(req, res)=>{
   
   try{
       const dID = req.user.id;
       req.logout();
       await User.destroy({where:{id:dID}});
       res.status(200).send({message:"User deleted"});
       
   }catch(e){
       res.status(500).send({message:e.message});
   }
    
});



//DELETE ONE PROJECT
app.delete("/projects/:id", loggedUser, async (req, res)=>{
   
   try{
       
       const foundProject = await Project.findOne({where:{id:req.params.id}});
       if(foundProject.UserId === req.user.id){
           
           await Project.destroy({where: {id:req.params.id}});
           res.status(200).send({message:"Project deleted"});
           
       }else{
           res.status(404).send({message:"Action denied"});
       }
       
   }catch(e){
       res.status(500).send({message:e.message});
   }
    
});



//DELETE ONE BUG
app.delete("/bugs/:id", loggedUser,async (req, res)=>{
   
   try{
       const foundBug = await Bug.findOne({where:{id:req.params.id}});
       if(foundBug){
           const foundProject = await Project.findOne({where:{id:foundBug.projectId}});
           if(foundProject){
               if(foundProject.UserId===req.user.id){
                   await Bug.destroy({where:{id:req.params.id}});
                   res.status(200).send({message:"Bug deleted!"});
               }
               else{
                   res.status(404).send({message:"Action denied!"});
               }
           }
           else{
               res.status(404).send({message:"The project was not found!"});
           }
       }else{
           res.status(404).send({message:"The bug was not found!"});
       }
       
   }catch(e){
       res.status(500).send({message:e.message});
   }
    
});


app.post("/pm",loggedUser, async (req,res)=>{
    console.log(req.body);
   try{
       
       const existingDEVMember = await ProjectMember.findOne({where:{UserId: req.body.UserId, is_tester: false}});
       const exitingTSTProject = await ProjectMember.findOne({where:{UserId: req.body.UserId, is_tester: true, projectId: req.body.pid}});
       
       if(!existingDEVMember && !exitingTSTProject){
            res.status(200).send(true);
       }
       else{
           res.status(404).send(false);
       }
   }catch(e){
       
       res.status(500).send({message:e.message});
       
   } 
});


app.listen(8080,()=>{
   console.log("Server started on port 8080..."); 
});