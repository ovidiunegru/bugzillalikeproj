import {EventEmitter} from "fbemitter";
import axios from "axios";
const SERVER = "http://18.224.73.15:8080";


class ProjectMain{
    
    constructor(){
        this.projects = {};
        this.emitter = new EventEmitter();
    }

    getProjects = () => {
        
        axios.get(`${SERVER}/projects`).then(projects =>{
                this.projects = projects.data;
                this.emitter.emit("GET_PROJECTS_SUCCESS");
        });
    }
    
}


export default ProjectMain;