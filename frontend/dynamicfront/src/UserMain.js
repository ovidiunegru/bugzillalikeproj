import {EventEmitter} from "fbemitter";
import axios from "axios";
const SERVER = "http://18.224.73.15:8080";

class UserMain{
    
    constructor(){
        this.user = null;
        this.emitter = new EventEmitter();
    };
    
    registerUser = (user) => {
        axios.post(`${SERVER}/register`, user).then(res => {
            this.user = res.data;
            this.emitter.emit("REGISTER_USER_SUCCESS");
        });
    };
    
    
    loginUser = (user) => {
        axios.post(`${SERVER}/login`,user).then(res => {
            this.user = res.data;
            this.emitter.emit("LOGIN_USER_SUCCESS");
        });
    };
    
    logoutUser = () => {
        axios.get(`${SERVER}/logout`).then(res => {
            this.user = null;
            this.emitter.emit("LOGOUT_USER_SUCCESS");
        })
    }
    
}

export default UserMain;