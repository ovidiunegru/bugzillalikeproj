const Sequelize = require("sequelize");
const sequelize = new Sequelize("dynamic","ovidiu","p@ss",{
    dialect:"mysql",
    define: {
        timestamps: false
  },
});
const passportLocalSequelize = require("passport-local-sequelize");


sequelize.authenticate().then(()=>{
    console.log("Database connection established!");
}).catch((err)=>{
    console.log(`Database connection error: ${err}`);
});



//try using passport
const User = passportLocalSequelize.defineUser(sequelize,{
   id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
        
    },
    username:{
        type: Sequelize.STRING,
        allowNULL: false,
        isEmail:true
    }
    
});



class Project extends Sequelize.Model{}
Project.init({
    
    id:{
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING,
        allowNULL: false
    },
    description:{
        type: Sequelize.STRING,
        allowNULL: false
    },
    repo_link:{
        type:Sequelize.STRING,
        allowNULL:false
    }
    
},{sequelize, modelName:"projects"});



class ProjectMember extends Sequelize.Model{}
ProjectMember.init({
   
   id:{
       type: Sequelize.INTEGER,
       autoIncrement: true,
       primaryKey: true
   },
   is_tester: {
       type:Sequelize.BOOLEAN,
       defaultValue: false
   }
   
},{sequelize, modelName:"projectMembers"});



class Bug extends Sequelize.Model{}
Bug.init({
    
    id:{
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey:true
    },
    id_dev:{
        type: Sequelize.INTEGER
    },
    id_tester:{
        type: Sequelize.INTEGER,
        allowNULL: false
    },
    severity:{
        type: Sequelize.STRING,
        allowNULL: false
    },
    priority:{
        type:Sequelize.STRING,
        allowNULL:false
    },
    description:{
        type: Sequelize.STRING,
        allowNULL: false
    },
    issue_link:{
        type: Sequelize.STRING,
        allowNULL: false
    },
    solved_link:{
        type: Sequelize.STRING
    },
    status:{
        type: Sequelize.STRING,
        allowNULL:false
    }
    
}, {sequelize,modelName:"bugs"});



Project.belongsTo(User);
Project.hasMany(Bug,{onDelete: "Cascade", hooks: true});
User.hasMany(ProjectMember, {onDelete: "Cascade", hooks: true});
Project.hasMany(ProjectMember,{onDelete: "Cascade", hooks: true});



//sequelize.sync({force:true});

module.exports = {sequelize, User, Project, ProjectMember, Bug};
